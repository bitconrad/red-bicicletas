let express = require('express');
let router = express.Router();
let passport = require('../config/passport');
const Usuario = require('../models/usuario');
const Token = require('../models/token');


const bcrypt = require('bcrypt');
const usuario = require('../models/usuario');
const token = require('../models/token');





router.get('/', function(req, res, next){
      res.render('session/login');
});


router.post('/', function(req, res, next){
    //passport
    

    passport.authenticate('local', function(err, usuario, info){
      //
     
      if(err) return next(err);
      if(!usuario) return res.render('session/login', {info});
     
      req.login(usuario, function(err){
        if (err) return next(err);
        return res.redirect('/');
      });
    })(req, res, next);
});

  

router.get('/logout', function(req, res, next){
    req.logout();
    res.redirect('/');
});

router.get('/forgotPassword', function(req, res, next){
    res.render('session/forgotPassword');
});

router.post('/forgotPassword', function(req, res, next){
    Usuario.findOne({email: req.body.email}, function(err, usuario){
      if (!usuario) return res.render('session/forgotPassword', {info: {message: 'No existe el usuario'}})
      
      Usuario.resetPasword(function(err){
        if(err) return next(err);
        console.log('session/forgotpasswordmessage');
      });

      res.render('session/forgotPasswordMessage');
    })
});

router.get('resetPassword/:token', function(req, res, next){
  Token.findOne({token: req.params.token}, function (err, token){
    if(!token) return res.status(400).send({ type:'not-verified', msg: 'No existe este token'})

    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({msg: 'No existe un usuario asociado a este token'})
      res.render('session/resetPassword', {errors: {}, usuario:usuario});
    });
  });
});


router.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', 
    {errors: {confirm_password: {message: 'No coinciden las contraseñas'}}, 
    usuario: new Usuario({email: req.body.email})});
    return;
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if(err){
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      } else {
        res.redirect('/login');
      }});
  });
})





module.exports = router;

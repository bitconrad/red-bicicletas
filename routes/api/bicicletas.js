let express = require('express');
let router = express.Router();

const mongoose = require('mongoose');
let bodyParser = require('body-parser');


let bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.use(bodyParser.json());

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/delete', bicicletaController.bicicleta_delete);
router.patch('/update', bicicletaController.bicicleta_update);

module.exports = router;

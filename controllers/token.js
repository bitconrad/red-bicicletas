let Usuario = require('../models/usuario');
let Token = require('../models/token');
const { usuario_reservar } = require('./api/usuarioControllerAPI');

module.exports = {
  confirmationGet: function(req, res, next){
   
    Token.findOne({ token: req.params.token }, function(err, token){
      
      

      if (!token) return res.status(400).send({ type: 'non-verified', msg: 'No se logro verficar'});
      Usuario.findById(token._userid, function(err, usuario){
        
        if (!usuario) {
          return res.status(400).send({ msg: 'No encontramos el usuario'});
        }
        if (usuario.verificado) return res.redirect('/usuarios');

     


        Usuario.findByIdAndUpdate(usuario.id, {verificado: true})
        .then(docs =>{
          console.log({userv: docs});
          if(err) { return res.status(500).send({ msg: err.message }); }
          res.redirect('/');
        });
      });
    });
  },
}

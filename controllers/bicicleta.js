let Bicicleta = require ('../models/bicicleta');
let mongoose = require ('mongoose');


exports.bicicleta_list = function(req, res, next) {
     Bicicleta.find({}, (err, bicicletas) =>{
       res.render('bicicletas/index', {bicicletas: bicicletas});
     });
}
    
 

exports.bicicleta_create_get = function(req, res, next) {
  res.render('bicicletas/create', {errors:{}, bicicleta: new Bicicleta()});
}


exports.bicicleta_create_post = function(req, res) {
  let bici= new Bicicleta({
    code: parseInt(req.body.code),
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [parseFloat(req.body.lat), parseFloat(req.body.lng)]
  });
  bici.save()
  .then(
    res.redirect('/bicicletas')
  )
  .catch(err => {
    console.log(err);
});
} 

  

exports.bicicleta_update_get = function(req, res, next) {
  Bicicleta.findById(req.params.id, (err, bicicleta) => {
    res.render('bicicletas/update', {errors:{}, bicicleta: bicicleta});
  });
  
}

exports.bicicleta_update_post = function(req, res, next) {
  let bici= {
      code: parseInt(req.body.code),
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [parseFloat(req.body.lat), parseFloat(req.body.lng)]
  };
  
  Bicicleta.findByIdAndUpdate(req.params.id, bici, function(err, bicicleta){
    console.log(req.params)
    if(err) {
      console.log(err);
      res.render('bicicletas/update');
    } else {
      res.redirect('/bicicletas');
      return;
    }
  })
}



exports.bicicleta_delete_post = function(req, res, next){
  Bicicleta.findByIdAndDelete(req.params.id, function(err){
    if(err)
      next(err);
    else
      res.redirect('/bicicletas');
  });
}
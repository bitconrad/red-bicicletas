let map = L.map('map').setView([4.580117153805364, -74.13174191567765], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    
}).addTo(map);

L.marker([4.6012424,-75.3061497]).addTo(map);
L.marker([4.5596932,-73.3808287]).addTo(map);
L.marker([4.599564,-76.3778777]).addTo(map);




$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  _success: function (result) {
    console.log(result);
    result.bicicletas.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici.code }).addTo(map);
    });
  },
  get success() {
    return this._success;
  },
  set success(value) {
    this._success = value;
  },
})

# 🚲**Red Bicicletas**🚲 

>Para este proyecto se utilizo: **NodeJs**, **ExpressJs** y **MongoDB** para montar una base de datos que usará la aplicación.

## **Informacion del Repositorio** ##
>Por comodidad, preferí usar gitlab.
El enlace del proyecto es:
```
 https://gitlab.com/bitconrad/red_bicicletas

```

### **Autor** ###

- Santiago Cortázar Molano
